package evaluator.controller.INT;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TestTopDown {
    public AppController appController;
    public IntrebariRepository intrebariRepository;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository(null);
        appController = new AppController(intrebariRepository);
        List<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add( new Intrebare("Cat face 1+1?","1)a","2)b","3)c","3","Stiinta"));
            intrebari.add(new Intrebare("Cat face 1+1?","1)a","2)b","3)c","3","Politica"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Stiinta"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Geografie"));
        } catch (InputValidationFailedException e) {
            assertTrue(false);
        }
        intrebariRepository.setIntrebari(intrebari);

    }
    @Test
    public void addNewIntrebareValida() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face 1+1?","1)2","2)3","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(false);
        }
        assertTrue((size+1) == intrebariRepository.getIntrebari().size());
    }
    @Test
    public void addIntrebareAndCreateTeste() {
        Collection<Intrebare> intrebari = new ArrayList<>();
        try {

            intrebari.add( new Intrebare("Cat face 1+1?","1)a","2)b","3)c","3","Matematica"));

            intrebari.add( new Intrebare("Cat face 1+1?","1)a","2)b","3)c","3","Geologie"));
        } catch (InputValidationFailedException e) {
            assertTrue(false);
        }
        intrebariRepository.getIntrebari().addAll(intrebari);
        assertTrue(intrebariRepository.getIntrebari().size() == 6);
        evaluator.model.Test test = null;
        try {
            test = appController.createNewTest();
        } catch (NotAbleToCreateTestException e) {
            assertTrue(false);
        }
        assertTrue(test.getIntrebari().size() == 5);
    }
    @Test
    public void intregrareTopDown(){
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face3 1+1?","1)2","2)3","3)4","3","Matematica");
            appController.addNewIntrebare("Cat face 1+1?","1)2","2)3","3)4","3","Geologie ");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(false);
        }
        assertTrue((size+2) == intrebariRepository.getIntrebari().size());
        evaluator.model.Test test = null;
        try {
            test = appController.createNewTest();
        } catch (NotAbleToCreateTestException e) {
            assertTrue(false);
        }
        assertTrue(test.getIntrebari().size() == 5);
        try {
            Statistica statistica = appController.getStatistica();
            assertTrue(statistica.getIntrebariDomenii().values().size() == 5);
            assertTrue( statistica.getIntrebariDomenii().get("Stiinta") == 2);
            assertTrue( statistica.getIntrebariDomenii().get("Geografie") == 1);
            assertTrue( statistica.getIntrebariDomenii().get("Politica") == 1);
            assertTrue( statistica.getIntrebariDomenii().get("Matematica") == 1);
            assertTrue( statistica.getIntrebariDomenii().get("Geologie ") == 1);
        } catch (NotAbleToCreateStatisticsException e) {
            assertTrue(false);
        }


    }
}
