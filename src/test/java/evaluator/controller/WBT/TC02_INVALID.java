package evaluator.controller.WBT;

import evaluator.controller.AppController;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TC02_INVALID {
    public AppController appController;
    public IntrebariRepository intrebariRepository;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository(null);
        appController = new AppController(intrebariRepository);
    }
    @Test
    public void createTestPutineIntrebari() {
        List<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add( new Intrebare("Ce ?","1)a","2)b","3)c","3","Stiinta"));
            intrebari.add(new Intrebare("De ce?","1)a","2)b","3)c","3","Politica"));

        } catch (InputValidationFailedException e) {
            assertTrue(false);
        }

        intrebariRepository.setIntrebari(intrebari);
        evaluator.model.Test test = null;
        try {
            test = appController.createNewTest();
        } catch (NotAbleToCreateTestException e) {
            if (e.getMessage().equals("Nu exista suficiente intrebari pentru crearea unui test!(5)"))
                assertTrue(true);
        }
        if(test != null){
            assertTrue(false);
        }
    }
}
