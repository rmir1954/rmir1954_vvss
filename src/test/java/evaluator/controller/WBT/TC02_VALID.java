package evaluator.controller.WBT;

import evaluator.controller.AppController;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TC02_VALID {
    public AppController appController;
    public IntrebariRepository intrebariRepository;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository(null);
        appController = new AppController(intrebariRepository);
    }
    @Test
    public void createTestValid() {
        List<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add( new Intrebare("Ce?","1)a","2)b","3)c","3","Stiinta"));
            intrebari.add(new Intrebare("De ce?","1)a","2)b","3)c","3","Politica"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Matematica"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Cultura"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Geografie"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Matematica"));

        } catch (InputValidationFailedException e) {
            assertTrue(false);
        }

        intrebariRepository.setIntrebari(intrebari);
        evaluator.model.Test test = null;
        try {
            test = appController.createNewTest();
        } catch (NotAbleToCreateTestException e) {
            assertTrue(false);
        }
        assertTrue(test.getIntrebari().size() == 5);
    }
}
