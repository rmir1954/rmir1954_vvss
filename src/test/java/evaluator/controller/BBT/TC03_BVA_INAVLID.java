package evaluator.controller.BBT;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TC03_BVA_INAVLID {
    public AppController appController;
    public IntrebariRepository intrebariRepository;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository(null);
        appController = new AppController(intrebariRepository);
    }
    @Test
    public void addNewIntrebareFaraMajuscula() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("cat face 1+1?","1)2","2)3","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(true);
        }
        assertTrue((size) == intrebariRepository.getIntrebari().size());
    }

}
