package evaluator.controller.BBT;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TC01_ECP_INVALID {
    public AppController appController;
    public IntrebariRepository intrebariRepository;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository(null);
        appController = new AppController(intrebariRepository);
    }
    @Test
    public void addNewIntrebareVar2Vida() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face 1+1?","1)2","","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().equals("Varianta2 este vida!"));
        }
        assertTrue((size) == intrebariRepository.getIntrebari().size());
    }
}
