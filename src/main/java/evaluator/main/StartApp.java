package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;

import evaluator.exception.NotAbleToCreateTestException;
import evaluator.gui.GUI;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.repository.IntrebariRepository;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "./src/main/resources/intrebari.txt";
	
	public static void main(String[] args){
		

		IntrebariRepository intrebariRepository = new IntrebariRepository(file);
		AppController appController = new AppController(intrebariRepository);
        GUI gui = new GUI(appController);
        gui.start();

		
	}

}
